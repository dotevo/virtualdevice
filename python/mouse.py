from Xlib import display
import sys

def hex2(n):
    return hex(n & 0xffffffff)

print("Mouse")

print("0000  0001") # EV_KEY
print("0001  0110")
print("0001  0111")
print("0001  0112")
print("0001  0113")
print("0001  0114")
print("0001  0118")
print("0001  0119")
print("0000  0002") # EV_REL
print("0002  0000") # REL_X
print("0002  0001") # REL_Y
print("0002  0006")
print("0002  0008")
x = None
y = None
print('q')
print('q')
sys.stdout.flush()
try:
    while True:
        data = display.Display().screen().root.query_pointer()._data
        if x != None and y != None and x != data["root_x"] and y != data["root_y"]:
            diff_x = data["root_x"] - x
            diff_y = data["root_y"] - y
            print("0002 0000 " + hex2(diff_x))
            print("0002 0001 " + hex2(diff_y))
            print("0000 0000 0000")
            sys.stdout.flush()
        x = data["root_x"]
        y = data["root_y"]
except KeyboardInterrupt:
    pass
