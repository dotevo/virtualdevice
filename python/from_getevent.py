import evdev
import sys
import argparse
import re
from subprocess import Popen, PIPE, STDOUT
import time
from helper import hex2,hexN

args = None

def arguments():
    global args
    parser = argparse.ArgumentParser(description='Virtual device')
    parser.add_argument('configFile', nargs='?', help='File with device configuration (getevent -p)')
    parser.add_argument('dataFile', nargs='?', help='File with device data (getevent -p)')
    parser.add_argument('--command', nargs='?', help='Command run for each device')
    parser.add_argument('--printstdout', action='store_true', help='Shows stdout of subprocesses')
    parser.add_argument('--printstderr', action='store_true', help='Shows stderr of subprocesses')


    parser.add_argument('--verbose', action='store_true', help='Verbose')
    parser.add_argument('--printConfig', action='store_true', help='Verbose')

    args = parser.parse_args()
    return parser

devices = {}

def parse_event(text):
    event = {}
    matchObj = re.finditer( r'([0-9a-f]{4})\s*(:\s*value\s([0-9]+),\s*min\s([-0-9]+),\s*max\s([0-9]+),\s*fuzz\s([0-9]+),\s*flat\s([0-9]+),\s*resolution\s([0-9]+))?', text, re.M)
    for p in matchObj:
        if p.group(2) is None:
            event[p.group(1)] = None
        else:
            event[p.group(1)] = {
                'value':p.group(3),
                'min':p.group(4),
                'max':p.group(5),
                'fuzz':p.group(6),
                'flat':p.group(7),
                'resolution':p.group(8),
            }

    return event

def parse_events(text):
    events = {}
    matchObj = re.finditer( r'([A-Z]+)\s*\(([0-9]*)\):([-a-z0-9\(\):\s,]*)', text, re.M)
    for p in matchObj:
        events[p.group(2)] = parse_event(p.group(3))

    return events

def parse_props(text):
    if text == 'INPUT_PROP_POINTER':
        return 0
    if text == 'INPUT_PROP_DIRECT':
        return 1
    if text == 'INPUT_PROP_BUTTONPAD':
        return 2
    if text == 'INPUT_PROP_SEMI_MT':
        return 3
    if text == 'INPUT_PROP_TOPBUTTONPAD':
        return 4
    if text == 'INPUT_PROP_POINTING_STICK':
        return 5
    if text == 'INPUT_PROP_ACCELEROMETER':
        return 6
    return 0

def add_device(text):
    global devices

    matchObj = re.match( r'\s*([0-9]+): (/[/a-z0-9]*)', text, re.M|re.I)
    if matchObj is None:
        return
    device = {}
    device['number'] = matchObj.group(1)
    device['path'] = matchObj.group(2)
    matchObj = re.search( r'name:\s*\"([a-zA-Z\-0-9\/_\.\s]*)\"', text, re.M)

    if matchObj is not None:
        device['name'] = matchObj.group(1)
    matchObj = re.search( r'events:([a-zA-Z0-9\/_\s():,-]*) input props', text, re.M)
    if matchObj is not None:
        device['events'] = parse_events(matchObj.group(1))
    matchObj = re.search( r'input props:\s*([A-Z_]*)', text, re.M)
    if matchObj is not None:
        device['props'] = parse_props(matchObj.group(1))

    devices[device['path']] = device
    print("New device: " + device['path'])

def loadConfiguration():
    fh = open(args.configFile)
    content = fh.read()

    text = content.split('add device')
    for p in text:
        add_device(p)
    fh.close()

prev_timestamp = 0
def sendEvent(t, path, group, key, value):
    if path not in devices:
        return
    global prev_timestamp
    if t:
        timestamp = float(t)
        if prev_timestamp == 0:
            prev_timestamp = timestamp
        p = timestamp - prev_timestamp
        if p > 0:
            time.sleep(p)
        prev_timestamp = timestamp


    p = devices[path]['process']
    if p:
        print("%s %s: %s %s %s"%(t, path, group, key, value))
        p.stdin.write("%s %s %s\n"%(group, key, value))
        try:
            p.stdin.flush()
        except:
            p_stdout,p_err = p.communicate()
            print("LOGS FROM: " + path)
            print(p_stdout)
            print("STDERR:")
            print(p_err)
    else:
        print('No process')

def parseData():
    fh = open(args.dataFile)
    content = fh.read()
    matchObj = re.finditer( r'(\[\s*([0-9.]+)\])?\s*(\/[\/a-z0-9]+):\s*([0-9a-z]+)\s*([0-9a-z]+)\s*([0-9a-z]+)', content, re.M)
    for p in matchObj:
        sendEvent(p.group(2), p.group(3), p.group(4), p.group(5), p.group(6))
    fh.close()

def printConfig():
    print("config")

def createProcesses():
    if args.command:
        for dev in devices:
            devices[dev]['process'] = Popen([args.command], shell=True, stdout=PIPE, stdin=PIPE, stderr=PIPE, encoding='utf8')

def configure():
    if args.command:
        for k in devices:
            dev = devices[k]
            p = dev['process']
            events = dev['events']
            str = dev['name'].replace(" ", "") + "\n"
            #events
            for ek in events:
                event = events[ek]
                str += "0000 " + ek + "\n"
                for ee in event:
                    str += "%s %s\n"%(ek, ee)
            str += "q\n"
            #abs
            if '0003' in events:
                for ep in events['0003']:
                    n = events['0003'][ep]

                    str += "%s %s %s %s %s\n"%(ep, hexN(n['min']), hexN(n['max']), hexN(n['fuzz']), hexN(n['flat']))

            str += "q\n"
            str += "%d\n"%dev['props']
            p.stdin.write(str)
            p.stdin.flush()
            print("***********************************")
            print(str)
            print("*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-")


if __name__ == "__main__":
    parser = arguments()
    loadConfiguration()
    if args.printConfig:
        printConfig()
        exit()

    createProcesses()
    configure()
    time.sleep(5)
    parseData()

    if args.command:
        for dev in devices:
            p = devices[dev]['process']
            print("Finished: " + devices[dev]['name'])
            p_stdout,p_err = p.communicate()
            if args.printstdout:
                print(p_stdout.strip())
            if args.printstderr:
                print(p_err.strip())
