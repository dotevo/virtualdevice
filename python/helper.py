def hex2(n):
    return hex(n & 0xffffffff)

def hexN(n):
    return format(int(n) & 0xffffffff, "08X")
