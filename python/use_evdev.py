import evdev
import sys
import argparse
from helper import hex2,hexN
args = None



def listDevices(args):
    devices = evdev.list_devices()
    for p in devices:
        print('Path: %s Device: %s' % (p,evdev.InputDevice(p).name))
        # TODO: It can be batter
        if args.verbose:
            print(p,evdev.InputDevice(p).capabilities())

def device(args):
    device = evdev.InputDevice(args.path)
    if args.grab:
        device.grab()

    print(device.name.replace(" ", ""))
    cap = device.capabilities()
    # print(cap)
    abs = []
    for p in cap:
        print("0000  " + hex2(p))
        for k in cap[p]:
            # is event
            if type(k) == int:
                print(hexN(p) + ' ' + hex2(k))
            else:
                print(hexN(p) + ' ' + hex2(k[0]))
                abs.append(k)

    sys.stdout.flush()
    print('q')

    # ABSes
    for p in abs:
        print("%s %s %s %s %s"%(hexN(p[0]), hex2(p[1].min)[2:], hex2(p[1].max)[2:], hex2(p[1].fuzz)[2:], hex2(p[1].flat)[2:]))
    print('q')
    print('0') # INPUT_PROP

    sys.stdout.flush()

    try:
        for ev in device.async_read_loop():
            print("%s %s %s"%(hexN(ev.type), hexN(ev.code), hexN(ev.value)))
            sys.stdout.flush()
    except KeyboardInterrupt:
        pass

    if args.grab:
        device.ungrab()

def arguments():
    global args
    parser = argparse.ArgumentParser(description='Virtual device')
    parser.add_argument('--verbose', action='store_true', help='Verbose')

    subparsers = parser.add_subparsers()
    list = subparsers.add_parser('list', help='list of devices')
    list.set_defaults(func=listDevices)

    dev = subparsers.add_parser('device', help='device')
    dev.add_argument('path', help='Path to the device')
    dev.add_argument('--grab', action='store_true', help='Grab device')

    dev.set_defaults(func=device)

    args = parser.parse_args()
    return parser

if __name__ == "__main__":
    parser = arguments()
    # Show help
    if not hasattr(args, 'func'):
        parser.print_help()
    else:
        args.func(args)
