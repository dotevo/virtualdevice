#include <stdio.h>
#include <iostream>
#include <vector>
#include <linux/uinput.h>
#include <unistd.h>
#include <fcntl.h>

using namespace std;

struct abs {
	int min;
	int max;
	int fuzz;
	int flat;
};

static int doIoctlSetInt(int fd, int request, int value, const char * tmp) {
	int rc = ioctl(fd, request, value);
	if (rc < 0) {
		cerr << hex << "ioctl(0x" << fd << ", 0x" << request << ", 0x" << value << ") = " << strerror(errno) << tmp << endl;
	}
	return rc;
}
/*
static int doIoctlGetInt(int fd, int request, int &result) {
	int rc = ioctl(fd, request, result);
	if (rc < 0) {
		cerr << hex << "ioctl(0x" << fd << ", 0x" << request << ") = " << strerror(errno) << endl;
	}
	return rc;
}
*/
int main() {
	//==========================  INIT PART  =============================

	// ------ NAME ------
	string name;
	cout << "Device name: " << endl;
	cin >> name;

	// Codes
	int type, code;
	vector<pair<int, int> > codes;
	cout << "Event 'type code' (q to finish): " << endl;

	while (cin >> hex >> type >> hex >> code) {
		codes.push_back(make_pair(type, code));
	}
	if (cin.fail()) {
		cin.clear();
		cin.ignore();
	}

	// ABSes
	cout << "ABS configs 'id min max fuzz flat' (q to finish): " << endl;
	int id;
	struct abs a;
	vector<pair<int, struct abs> > abses;
	long min, max, fuzz, flat;
	while (cin >> hex >> id >>
								hex >> min >>
								hex >> max >>
								hex >> fuzz >>
								hex >> flat) {

		a.min = min;
		a.max = max;
		a.fuzz = fuzz;
		a.flat = flat;
		abses.push_back(make_pair(id, a));
	}
	if (cin.fail()) {
		cin.clear();
		cin.ignore();
	}

	uint_t prop = 0;
	cout << "Prop' (q to skip): " << endl;
	cin >> prop;
	if (cin.fail()) {
		cin.clear();
		cin.ignore();
	}

	//=========================== CREATE =============================

	int fd = open("/dev/uinput", O_WRONLY | O_NDELAY);
	//int version;
	//doIoctlGetInt(fd, UI_GET_VERSION, version);
	cout << "Name: " << name << endl;

	// TODO: this should be made configurable
	//#define INPUT_PROP_POINTER 0x00
	//#define INPUT_PROP_DIRECT 0x01
	//#define INPUT_PROP_BUTTONPAD 0x02
	//#define INPUT_PROP_SEMI_MT 0x03
	//#define INPUT_PROP_TOPBUTTONPAD 0x04
	//#define INPUT_PROP_POINTING_STICK 0x05
	//#define INPUT_PROP_ACCELEROMETER 0x06
	//#define INPUT_PROP_MAX 0x1f
	//#define INPUT_PROP_CNT (INPUT_PROP_MAX + 1)
	cout << ":PROP " << INPUT_PROP_DIRECT << ":" << prop << endl;
	doIoctlSetInt(fd, UI_SET_PROPBIT, prop, "cycki");

	cout << "Events: " << codes.size() << endl;
	for (unsigned long i = 0; i < codes.size(); i++) {
		cout << "Events: " << codes[i].first << endl;
		doIoctlSetInt(fd, codes[i].first + UI_SET_EVBIT, codes[i].second, "dupa");
	}

	struct uinput_user_dev usetup;
	memset(&usetup, 0, sizeof(uinput_user_dev));
	usetup.id.bustype = BUS_VIRTUAL;
	usetup.id.version = 1;
	cout << "ABSes: " << abses.size() << endl;
	for (unsigned long i = 0; i < abses.size(); i++) {
		cout << "ABS: " << abses[i].first << endl;
		usetup.absmax[abses[i].first] = abses[i].second.max;
		usetup.absmin[abses[i].first] = abses[i].second.min;
		usetup.absfuzz[abses[i].first] = abses[i].second.fuzz;
		usetup.absflat[abses[i].first] = abses[i].second.flat;
	}

	strcpy(usetup.name, name.c_str());

	if (write(fd, &usetup, sizeof(usetup)) != sizeof(usetup)) {
		return 1;
	}

	if (ioctl(fd, UI_DEV_CREATE) < 0) {
		cerr << "udev creation failed" << endl;
		return 1;
	}

	// SEND EVENTS
	struct input_event ie;
	ie.time.tv_sec = 0;
	ie.time.tv_usec = 0;
	cout << "Event type code value (q to finish): " << endl;
	long p = 0;
	while (cin >> hex >> ie.type >>
						 hex >> ie.code >>
						 hex >> p) {

		ie.value = p;

		if (ie.type == 0) {
			cout << "sync! " << endl;
		}
		int rc = write(fd, &ie, sizeof(ie));
		if (rc < 0 || rc < (int) sizeof(ie))
			cout << "RC:" << rc << ":" << endl;
	}
	if (cin.fail()) {
		cout << "FAILED" << endl;
		string s;
		cin >> s;
		cout << s << ":";
		cin >> s;
		cout << s<< ":";
		cin >> s;
		cout << s << ":";
		cin >> s;
		cout << s << ":";
	}

	cout << "Bye" << endl;
	return 0;
}
